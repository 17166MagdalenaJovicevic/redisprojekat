import { Blanket } from "./blanket.js";

sessionStorage.setItem("webapi","https://localhost:5001/");
var webapi = sessionStorage.getItem("webapi");

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
let godinaPredavanja = urlParams.get('godinaPredavanja');
let predmet = urlParams.get('predmet');
let godina = urlParams.get('godina');
let rok = urlParams.get('rok');

VratiBlanket(godinaPredavanja,predmet,godina,rok);

async function VratiBlanket(godinaPredavanja, predmet, godina, rok) {

    if (godinaPredavanja == "" || predmet == "" || godina == "" || rok == "") {
        return false;
    }

    let blanket = await FetchVratiBlanket(godinaPredavanja, predmet, godina, rok);

    if (blanket == false) {
        return false;
    }

    let kontejner = document.querySelector(".Kontejner");
    blanket.PrikazIzmena(kontejner);

}

async function FetchVratiBlanket(godinaPredavanja, predmet, godina, rok) {

    let odgovor = await fetch(webapi + "Student/VratiBlanket/" +godinaPredavanja+"/"+predmet + "/" + godina + "/" + rok, {
        method: 'GET',
        headers: {

        }
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.json().catch(reason => false);

    if (odgovor == false || odgovor == null) {
        return false;
    }

    let b = odgovor;


    return new Blanket(b.godinaPredavanja, b.predmet, b.godina, b.rok, b.fajlovi, b.putanjeDoFajlova);



    
}