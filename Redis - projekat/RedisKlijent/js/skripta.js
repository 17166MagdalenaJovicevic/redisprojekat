export class Skripta{

    constructor(naziv,godinaPredavanja, predmet, fajlovi, imenaFajlova, autor, ticksDatum ) {

        this.naziv = naziv;
        this.godinaPredavanja = godinaPredavanja;
        this.predmet = predmet;
        this.imenaFajlova = imenaFajlova;
        this.fajlovi = fajlovi;
        this.ticksDatum = ticksDatum;
        this.autor = autor;
    }

    PrikazSkripte(host) {

        let kontejner = document.createElement("div");
        kontejner.classList.add("p-2","m-2","rounded","d-flex","flex-column", "bg-secondary");

        let datum = document.createElement("div");
        var epochMicrotimeDiff = new Date(1970,1,1).getTime() - new Date(1,1,1).getTime();
        let dtm = new Date(Math.floor(parseInt(this.ticksDatum)/10000)-epochMicrotimeDiff);
        datum.innerHTML = dtm.getDate() + ". " + (dtm.getMonth() + 1) + ". " + dtm.getYear() + ".";
        kontejner.appendChild(datum);

        let nazivDiv = document.createElement("div");
        nazivDiv.innerHTML = "Naziv skripte: " + this.naziv;
        kontejner.appendChild(nazivDiv);

        let autorDiv = document.createElement("div");
        autorDiv.innerHTML = "Autor: " + this.autor;
        kontejner.appendChild(autorDiv);

        let godinaPredavanjaDiv = document.createElement("div");
        godinaPredavanjaDiv.innerHTML = this.godinaPredavanja;
        kontejner.appendChild(godinaPredavanjaDiv);

        let predmetDiv = document.createElement("div");
        predmetDiv.innerHTML = this.predmet;
        kontejner.appendChild(predmetDiv);

        

        // deo za fajlove

        let naslov = document.createElement("h5");
        naslov.innerHTML = "Fajlovi";
        kontejner.appendChild(naslov);

        let kontejnerZaFajlove = document.createElement("div");
        kontejnerZaFajlove.classList.add("p-1","d-flex","flex-column");

        this.fajlovi.forEach((f,i) => {

            let div = document.createElement("div");


            let str = this.imenaFajlova[i].split(".");
            let ekstenzija = str[str.length - 1];
            let blob;
            let fajl = this.base64ToArrayBuffer(f);
            if (ekstenzija == "txt") {
                blob = new Blob([fajl], {type: "text/plain"});
            }
            else if (ekstenzija == "pdf") {
                blob = new Blob([fajl], {type: "application/pdf"});
            }
            else if (ekstenzija == "doc" || ekstenzija == "docx") {
                blob = new Blob([fajl], {type: "application/vnd.openxmlformats-officedocument.wordprocessingml"});
            }
            else if (ekstenzija == "jpg" || ekstenzija == "jpeg") {
                blob = new Blob([fajl], {type: "image/jpeg"});
            }
            else if (ekstenzija == "png") {
                blob = new Blob([fajl], {type: "image/png"});
            }
            
            let link = document.createElement("a");
            link.target = "_blank";
            link.innerHTML = " " + this.imenaFajlova[i];
            link.download = this.imenaFajlova[i];
            link.href = window.URL.createObjectURL(blob);

            div.appendChild(link);


            kontejnerZaFajlove.appendChild(div);

            if (ekstenzija == "jpg" || ekstenzija == "png" || ekstenzija == "jpeg") {
                let iframe = document.createElement("iframe");
                iframe.classList.add("p-2");
                iframe.src = window.URL.createObjectURL(blob);
                kontejnerZaFajlove.appendChild(iframe);
                iframe.height = "1000px";
            }
            
            

        })

        

        kontejner.appendChild(kontejnerZaFajlove);

        if (localStorage.getItem("token") != null) {
            let link = document.createElement("button");
            link.classList.add("btn","btn-danger");
            link.innerHTML = "Obriši";
            link.onclick = (ev) => {
                this.ObrisiSkriptu();
            }
            kontejner.appendChild(link);
        }

        host.appendChild(kontejner);

    }

    PrikazNepotvrdjeno(host) {

        let kontejner = document.createElement("div");
        kontejner.classList.add("p-2","m-2","rounded","d-flex","flex-column", "bg-secondary");

        let nazivDiv = document.createElement("div");
        nazivDiv.innerHTML = this.naziv;
        kontejner.appendChild(nazivDiv);

        let godinaPredavanjaDiv = document.createElement("div");
        godinaPredavanjaDiv.innerHTML = this.godinaPredavanja;
        kontejner.appendChild(godinaPredavanjaDiv);

        let predmetDiv = document.createElement("div");
        predmetDiv.innerHTML = "Predmet: " + this.predmet;
        kontejner.appendChild(predmetDiv);

        let autorDiv = document.createElement("div");
        autorDiv.innerHTML = "Autor skripte: " + this.autor;
        kontejner.appendChild(autorDiv);

        // deo za fajlove

        let naslov = document.createElement("h5");
        naslov.innerHTML = "Fajlovi";
        kontejner.appendChild(naslov);

        let kontejnerZaFajlove = document.createElement("div");
        kontejnerZaFajlove.classList.add("p-1","d-flex","flex-column");

        this.fajlovi.forEach((f,i) => {

            let div = document.createElement("div");

            let cbx = document.createElement("input");
            cbx.type = "checkbox";
            cbx.value = this.imenaFajlova[i];

            div.appendChild(cbx);

            let str = this.imenaFajlova[i].split(".");
            let ekstenzija = str[str.length - 1];
            let blob;
            let fajl = this.base64ToArrayBuffer(f);
            if (ekstenzija == "txt") {
                blob = new Blob([fajl], {type: "text/plain"});
            }
            else if (ekstenzija == "pdf") {
                blob = new Blob([fajl], {type: "application/pdf"});
            }
            else if (ekstenzija == "doc" || ekstenzija == "docx") {
                blob = new Blob([fajl], {type: "application/vnd.openxmlformats-officedocument.wordprocessingml"});
            }
            else if (ekstenzija == "jpg" || ekstenzija == "jpeg") {
                blob = new Blob([fajl], {type: "image/jpeg"});
            }
            else if (ekstenzija == "png") {
                blob = new Blob([fajl], {type: "image/png"});
            }
            
            let link = document.createElement("a");
            link.innerHTML = " " + this.imenaFajlova[i];
            link.download = this.imenaFajlova[i];
            link.href = window.URL.createObjectURL(blob);

            div.appendChild(link);

            kontejnerZaFajlove.appendChild(div);
        })

        kontejner.appendChild(kontejnerZaFajlove);


        let dugmePotvrdi = document.createElement("button");
        dugmePotvrdi.classList.add("btn", "m-2");
        dugmePotvrdi.innerHTML = "Potvrdi skriptu";
        kontejner.appendChild(dugmePotvrdi);

        dugmePotvrdi.onclick = (ev) => {

            let listaFajlova = [];

            let cbxs = kontejner.querySelectorAll("input[type=checkbox]");
            cbxs.forEach(cbx => {
                if (cbx.checked) {
                    listaFajlova.push(cbx.value);
                }
            })
            
            this.PosaljiSkriptu(listaFajlova, kontejner)

        }


        host.appendChild(kontejner);

    }

    base64ToArrayBuffer(base64) {

        var binaryString = window.atob(base64);
        var binaryLen = binaryString.length;
        var bytes = new Uint8Array(binaryLen);
        for (var i = 0; i < binaryLen; i++) {
           var ascii = binaryString.charCodeAt(i);
           bytes[i] = ascii;
        }
        return bytes;
    }

    PosaljiSkriptu(listaFajlova, kontejner) {

        this.FetchPosaljiSkriptu(listaFajlova, kontejner);

    }

    async FetchPosaljiSkriptu(listaFajlova, kontejner) {

        let odgovor = await fetch(sessionStorage.getItem("webapi")+"Administrator/DodajSkriptu/"+ this.ticksDatum + "/" + this.godinaPredavanja+"/"+this.predmet , {
            method:"POST",
            headers:{
                "Content-Type":"application/json",
                "Authorization": localStorage.getItem("token")
            },
            body: JSON.stringify(listaFajlova)
        }).catch(reason=>{
            return false;
        })
    
        if (odgovor == false){
            return false;
        }
    
        odgovor = await odgovor.text().catch(reason => false);
    
        if (odgovor == false){
            return false;
        }
    
        if (odgovor == "Uspesno") {
            alert("Uspešno potvrdjena skripta - " + this.naziv + " " + this.godinaPredavanja + " " + this.predmet + " " + this.autor);
            document.querySelector(".NepotvrdjeneSkripte").removeChild(kontejner);
            
        } 

        
    }



    async ObrisiSkriptu() {

        let odgovor = await fetch(sessionStorage.getItem("webapi")+"Administrator/ObrisiSkriptu/"+ this.ticksDatum + "/" + this.godinaPredavanja+"/"+this.predmet , {
            method:"POST",
            headers:{
                "Content-Type":"application/json",
                "Authorization": localStorage.getItem("token")
            }
        }).catch(reason=>{
            return false;
        })
    
        if (odgovor == false){
            return false;
        }
    
        odgovor = await odgovor.text().catch(reason => false);
    
        if (odgovor == false){
            return false;
        }
    
        if (odgovor == "Uspesno") {
            window.location.reload();
            
        } 

    }

}