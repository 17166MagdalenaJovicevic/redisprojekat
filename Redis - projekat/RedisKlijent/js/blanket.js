export class Blanket {

    constructor(godinaPredavanja, predmet, godina, rok, fajlovi, imenaFajlova) {

        this.godinaPredavanja = godinaPredavanja;
        this.predmet = predmet;
        this.godina = godina;
        this.rok = rok;
        this.imenaFajlova = imenaFajlova;
        this.fajlovi = fajlovi;

    }

    PrikazBlanketa(host) {

        let kontejner = document.createElement("div");
        kontejner.classList.add("p-2","m-2","rounded","d-flex","flex-column", "bg-secondary");

        let godinaPredavanjaDiv = document.createElement("div");
        godinaPredavanjaDiv.innerHTML = this.godinaPredavanja;
        kontejner.appendChild(godinaPredavanjaDiv);

        let predmetDiv = document.createElement("div");
        predmetDiv.innerHTML = this.predmet;
        kontejner.appendChild(predmetDiv);

        let rokgodinaDiv = document.createElement("div");
        rokgodinaDiv.innerHTML = this.godina + " " + this.rok + " rok"
        kontejner.appendChild(rokgodinaDiv);

        // deo za fajlove

        let naslov = document.createElement("h5");
        naslov.innerHTML = "Fajlovi";
        kontejner.appendChild(naslov);

        let kontejnerZaFajlove = document.createElement("div");
        kontejnerZaFajlove.classList.add("p-1","d-flex","flex-column");

        this.fajlovi.forEach((f,i) => {

            let div = document.createElement("div");


            let str = this.imenaFajlova[i].split(".");
            let ekstenzija = str[str.length - 1];
            let blob;
            let fajl = this.base64ToArrayBuffer(f);
            if (ekstenzija == "txt") {
                blob = new Blob([fajl], {type: "text/plain"});
            }
            else if (ekstenzija == "pdf") {
                blob = new Blob([fajl], {type: "application/pdf"});
            }
            else if (ekstenzija == "doc" || ekstenzija == "docx") {
                blob = new Blob([fajl], {type: "application/vnd.openxmlformats-officedocument.wordprocessingml"});
            }
            else if (ekstenzija == "jpg" || ekstenzija == "jpeg") {
                blob = new Blob([fajl], {type: "image/jpeg"});
            }
            else if (ekstenzija == "png") {
                blob = new Blob([fajl], {type: "image/png"});
            }
            
            let link = document.createElement("a");
            link.target = "_blank";
            link.innerHTML = " " + this.imenaFajlova[i];
            link.download = this.imenaFajlova[i];
            link.href = window.URL.createObjectURL(blob);

            div.appendChild(link);


            kontejnerZaFajlove.appendChild(div);

            if (ekstenzija == "jpg" || ekstenzija == "png" || ekstenzija == "jpeg") {
                let iframe = document.createElement("iframe");
                iframe.classList.add("p-2");
                iframe.src = window.URL.createObjectURL(blob);
                kontejnerZaFajlove.appendChild(iframe);
                iframe.height = "1000px";
            }
            

        })

        

        kontejner.appendChild(kontejnerZaFajlove);

        if (localStorage.getItem("token") != null) {
            let link = document.createElement("a");
            link.innerHTML = "Izmeni";
            link.target = "_blank";
            link.rel = "noopener noreferrer";
            link.href = "izmeniBlanket.html?godinaPredavanja=" + this.godinaPredavanja + "&predmet=" + this.predmet + "&godina=" + this.godina + "&rok=" + this.rok;
            kontejner.appendChild(link);
        }

        host.appendChild(kontejner);

    }

    PrikazNepotvrdjeno(host) {

        let kontejner = document.createElement("div");
        kontejner.classList.add("p-2","m-2","rounded","d-flex","flex-column", "bg-secondary");

        let godinaPredavanjaDiv = document.createElement("div");
        godinaPredavanjaDiv.innerHTML = this.godinaPredavanja;
        kontejner.appendChild(godinaPredavanjaDiv);

        let predmetDiv = document.createElement("div");
        predmetDiv.innerHTML = this.predmet;
        kontejner.appendChild(predmetDiv);

        let rokgodinaDiv = document.createElement("div");
        rokgodinaDiv.innerHTML = this.godina + " " + this.rok + " rok"
        kontejner.appendChild(rokgodinaDiv);

        // deo za fajlove

        let naslov = document.createElement("h5");
        naslov.innerHTML = "Fajlovi";
        kontejner.appendChild(naslov);

        let kontejnerZaFajlove = document.createElement("div");
        kontejnerZaFajlove.classList.add("p-1","d-flex","flex-column");

        this.fajlovi.forEach((f,i) => {

            let div = document.createElement("div");

            let cbx = document.createElement("input");
            cbx.type = "checkbox";
            cbx.value = this.imenaFajlova[i];

            div.appendChild(cbx);

            let str = this.imenaFajlova[i].split(".");
            let ekstenzija = str[str.length - 1];
            let blob;
            let fajl = this.base64ToArrayBuffer(f);
            if (ekstenzija == "txt") {
                blob = new Blob([fajl], {type: "text/plain"});
            }
            else if (ekstenzija == "pdf") {
                blob = new Blob([fajl], {type: "application/pdf"});
            }
            else if (ekstenzija == "doc" || ekstenzija == "docx") {
                blob = new Blob([fajl], {type: "application/vnd.openxmlformats-officedocument.wordprocessingml"});
            }
            else if (ekstenzija == "jpg" || ekstenzija == "jpeg") {
                blob = new Blob([fajl], {type: "image/jpeg"});
            }
            else if (ekstenzija == "png") {
                blob = new Blob([fajl], {type: "image/png"});
            }
            
            let link = document.createElement("a");
            link.innerHTML = " " + this.imenaFajlova[i];
            link.download = this.imenaFajlova[i];
            link.href = window.URL.createObjectURL(blob);

            div.appendChild(link);

            kontejnerZaFajlove.appendChild(div);
        })

        kontejner.appendChild(kontejnerZaFajlove);


        let dugmePotvrdi = document.createElement("button");
        dugmePotvrdi.classList.add("btn", "m-2");
        dugmePotvrdi.innerHTML = "Potvrdi blanket";
        kontejner.appendChild(dugmePotvrdi);

        dugmePotvrdi.onclick = (ev) => {

            let listaFajlova = [];

            let cbxs = kontejner.querySelectorAll("input[type=checkbox]");
            cbxs.forEach(cbx => {
                if (cbx.checked) {
                    listaFajlova.push(cbx.value);
                }
            })
            
            this.PosaljiBlanket(listaFajlova, kontejner)

        }


        host.appendChild(kontejner);

    }

    PrikazIzmena(host) {

        let kontejner = document.createElement("div");
        kontejner.classList.add("p-2","m-2","rounded","d-flex","flex-column", "bg-secondary");

        let godinaPredavanjaDiv = document.createElement("div");
        godinaPredavanjaDiv.innerHTML = this.godinaPredavanja;
        kontejner.appendChild(godinaPredavanjaDiv);

        let predmetDiv = document.createElement("div");
        predmetDiv.innerHTML = this.predmet;
        kontejner.appendChild(predmetDiv);

        let rokgodinaDiv = document.createElement("div");
        rokgodinaDiv.innerHTML = this.godina + " " + this.rok + " rok"
        kontejner.appendChild(rokgodinaDiv);

        // deo za fajlove

        let naslov = document.createElement("h5");
        naslov.innerHTML = "Fajlovi";
        kontejner.appendChild(naslov);

        let kontejnerZaFajlove = document.createElement("div");
        kontejnerZaFajlove.classList.add("p-1","d-flex","flex-column");

        this.fajlovi.forEach((f,i) => {

            let div = document.createElement("div");

            let cbx = document.createElement("input");
            cbx.type = "checkbox";
            cbx.value = this.imenaFajlova[i];
            cbx.checked = true;

            div.appendChild(cbx);

            let str = this.imenaFajlova[i].split(".");
            let ekstenzija = str[str.length - 1];
            let blob;
            let fajl = this.base64ToArrayBuffer(f);
            if (ekstenzija == "txt") {
                blob = new Blob([fajl], {type: "text/plain"});
            }
            else if (ekstenzija == "pdf") {
                blob = new Blob([fajl], {type: "application/pdf"});
            }
            else if (ekstenzija == "doc" || ekstenzija == "docx") {
                blob = new Blob([fajl], {type: "application/vnd.openxmlformats-officedocument.wordprocessingml"});
            }
            else if (ekstenzija == "jpg" || ekstenzija == "jpeg") {
                blob = new Blob([fajl], {type: "image/jpeg"});
            }
            else if (ekstenzija == "png") {
                blob = new Blob([fajl], {type: "image/png"});
            }
            
            let link = document.createElement("a");
            link.innerHTML = " " + this.imenaFajlova[i];
            link.download = this.imenaFajlova[i];
            link.href = window.URL.createObjectURL(blob);

            div.appendChild(link);

            kontejnerZaFajlove.appendChild(div);
        })

        kontejner.appendChild(kontejnerZaFajlove);


        let dugmePotvrdi = document.createElement("button");
        dugmePotvrdi.classList.add("btn", "m-2");
        dugmePotvrdi.innerHTML = "Izmeni blanket";
        kontejner.appendChild(dugmePotvrdi);

        dugmePotvrdi.onclick = (ev) => {

            let listaFajlova = [];

            let cbxs = kontejner.querySelectorAll("input[type=checkbox]");
            cbxs.forEach(cbx => {
                if (cbx.checked) {
                    listaFajlova.push(cbx.value);
                }
            })
            
            this.IzmeniBlanket(listaFajlova, kontejner)

        }


        host.appendChild(kontejner);

    }

    base64ToArrayBuffer(base64) {

        var binaryString = window.atob(base64);
        var binaryLen = binaryString.length;
        var bytes = new Uint8Array(binaryLen);
        for (var i = 0; i < binaryLen; i++) {
           var ascii = binaryString.charCodeAt(i);
           bytes[i] = ascii;
        }
        return bytes;
    }

    PosaljiBlanket(listaFajlova, kontejner) {

        this.FetchPosaljiBlanket(listaFajlova, kontejner);

    }

    async FetchPosaljiBlanket(listaFajlova, kontejner) {

        let odgovor = await fetch(sessionStorage.getItem("webapi")+"Administrator/DodajBlanket/"+this.godinaPredavanja+"/"+this.predmet + "/" + this.godina + "/" + this.rok, {
            method:"POST",
            headers:{
                "Content-Type":"application/json",
                "Authorization": localStorage.getItem("token")
            },
            body: JSON.stringify(listaFajlova)
        }).catch(reason=>{
            return false;
        })
    
        if (odgovor == false){
            return false;
        }
    
        odgovor = await odgovor.text().catch(reason => false);
    
        if (odgovor == false){
            return false;
        }
    
        if (odgovor == "Uspesno") {
            alert("Uspešno potvrdjen blanket - " + this.godinaPredavanja + " " + this.predmet + ' ' + this.godina + " " + this.rok + " rok");
            document.querySelector(".NepotvrdjeniBlanketi").removeChild(kontejner);
            
        } 

        
    }


    IzmeniBlanket(listaFajlova, kontejner) {

        this.FetchIzmeniBlanket(listaFajlova, kontejner);

    }

    async FetchIzmeniBlanket(listaFajlova, kontejner) {

        let odgovor = await fetch(sessionStorage.getItem("webapi")+"Administrator/IzmeniBlanket/"+this.godinaPredavanja+"/"+this.predmet + "/" + this.godina + "/" + this.rok, {
            method:"POST",
            headers:{
                "Content-Type":"application/json",
                "Authorization": localStorage.getItem("token")
            },
            body: JSON.stringify(listaFajlova)
        }).catch(reason=>{
            return false;
        })
    
        if (odgovor == false){
            return false;
        }
    
        odgovor = await odgovor.text().catch(reason => false);
    
        if (odgovor == false){
            return false;
        }
    
        if (odgovor == "Uspesno") {
            alert("Uspešno izmenjen blanket - " + this.godinaPredavanja + " " + this.predmet + ' ' + this.godina + " " + this.rok + " rok");
            window.location.reload();
        } 

        
    }

}
