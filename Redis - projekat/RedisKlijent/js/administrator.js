import {Blanket} from "./blanket.js";
import {Skripta} from "./skripta.js";
import {Beleska} from "./beleska.js";

sessionStorage.setItem("webapi","https://localhost:5001/");
var webapi = sessionStorage.getItem("webapi");

Inicijalizacija();

async function Inicijalizacija() {

    await VratiNepotvrdjeneBlankete();
    await VratiNepotvrdjeneSkripte();
    await VratiNepotvrdjeneBeleske();

}

async function VratiNepotvrdjeneBlankete() {

    let lista = await FetchVratiNepotvrdjeneBlankete();

    if (lista == false) {
        return false;
    }

    let kontejner = document.querySelector(".NepotvrdjeniBlanketi");

    lista.forEach(blanket => {
        blanket.PrikazNepotvrdjeno(kontejner);
    })

}

async function FetchVratiNepotvrdjeneBlankete() {

    let odgovor = await fetch(webapi + "Administrator/VratiNepotvrdjeneBlankete", {
        method: 'GET',
        headers: {
            'Authorization' : localStorage.getItem("token")
        }
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.json().catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    let lista = [];

    odgovor.forEach(b => {
        lista.push(new Blanket(b.godinaPredavanja, b.predmet, b.godina, b.rok, b.fajlovi, b.putanjeDoFajlova));
    })

    return lista;
    
}

async function VratiNepotvrdjeneSkripte() {

    let lista = await FetchVratiNepotvrdjeneSkripte();

    if (lista == false) {
        return false;
    }

    let kontejner = document.querySelector(".NepotvrdjeneSkripte");

    lista.forEach(skripta => {
        skripta.PrikazNepotvrdjeno(kontejner);
    })

}

async function FetchVratiNepotvrdjeneSkripte() {

    let odgovor = await fetch(webapi + "Administrator/VratiNepotvrdjeneSkripte", {
        method: 'GET',
        headers: {
            'Authorization' : localStorage.getItem("token")
        }
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.json().catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    let lista = [];

    odgovor.forEach(b => {
        lista.push(new Skripta(b.naziv, b.godinaPredavanja, b.predmet , b.fajlovi, b.putanjeDoFajlova, b.autor, b.ticksDatumString));
    })

    return lista;
}

async function VratiNepotvrdjeneBeleske() {

    let lista = await FetchVratiNepotvrdjeneBeleske();

    if (lista == false) {
        return false;
    }

    let kontejner = document.querySelector(".NepotvrdjeneBeleske");

    lista.forEach(skripta => {
        skripta.PrikazNepotvrdjeno(kontejner);
    })

}

async function FetchVratiNepotvrdjeneBeleske() {

    let odgovor = await fetch(webapi + "Administrator/VratiNepotvrdjeneBeleske", {
        method: 'GET',
        headers: {
            'Authorization' : localStorage.getItem("token")
        }
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.json().catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    let lista = [];

    odgovor.forEach(b => {
        lista.push(new Beleska(b.naziv, b.godinaPredavanja, b.predmet , b.fajlovi, b.putanjeDoFajlova, b.autor, b.ticksDatumString));
    })

    return lista;
}


document.querySelector(".DodajPredmet").onclick = (ev) => {
    DodajPredmet();
}

document.querySelector(".UkloniPredmet").onclick = (ev) => {
    UkloniPredmet();
}

document.querySelector(".DodajRok").onclick = (ev) => {
    DodajRok();
}

document.querySelector(".UkloniRok").onclick = (ev) => {
    UkloniRok();
}


async function DodajPredmet() {

    let godinaPredavanja = document.querySelector(".GodinePredavanja").value;

    let naziv = document.querySelector("input[name=NazivPredmeta]").value;

    if (naziv == "")
        return;

    let odgovor = await fetch(webapi + "Administrator/DodajPredmet/" + godinaPredavanja + "/" + naziv, {
        method: 'POST',
        headers: {
            'Authorization' : localStorage.getItem("token")
        }
    }).catch(reason => false);
    
    if (odgovor == false) {
        return false;
    }
    
    odgovor = await odgovor.text().catch(reason => false);
    
    if (odgovor == false) {
        return false;
    }
    
    if (odgovor == "Uspesno") {
        alert("Uspešno dodat predmet!");
    }
    
}


async function UkloniPredmet() {

    let godinaPredavanja = document.querySelector(".GodinePredavanja").value;

    let naziv = document.querySelector("input[name=NazivPredmeta]").value;

    if (naziv == "")
        return;

    let odgovor = await fetch(webapi + "Administrator/UkloniPredmet/" + godinaPredavanja + "/" + naziv, {
        method: 'POST',
        headers: {
            'Authorization' : localStorage.getItem("token")
        }
    }).catch(reason => false);
    
    if (odgovor == false) {
        return false;
    }
    
    odgovor = await odgovor.text().catch(reason => false);
    
    if (odgovor == false) {
        return false;
    }
    
    if (odgovor == "Uspesno") {
        alert("Uspešno uklonjen predmet!");
    }
    
}


async function DodajRok() {


    let naziv = document.querySelector("input[name=NazivRoka]").value;

    if (naziv == "")
        return;

    let odgovor = await fetch(webapi + "Administrator/DodajRok/" + naziv, {
        method: 'POST',
        headers: {
            'Authorization' : localStorage.getItem("token")
        }
    }).catch(reason => false);
    
    if (odgovor == false) {
        return false;
    }
    
    odgovor = await odgovor.text().catch(reason => false);
    
    if (odgovor == false) {
        return false;
    }
    
    if (odgovor == "Uspesno") {
        alert("Uspešno dodat rok!");
    }
    
}


async function UkloniRok() {

    let naziv = document.querySelector("input[name=NazivRoka]").value;

    if (naziv == "")
        return;

    let odgovor = await fetch(webapi + "Administrator/UkloniRok/" + naziv, {
        method: 'POST',
        headers: {
            'Authorization' : localStorage.getItem("token")
        }
    }).catch(reason => false);
    
    if (odgovor == false) {
        return false;
    }
    
    odgovor = await odgovor.text().catch(reason => false);
    
    if (odgovor == false) {
        return false;
    }
    
    if (odgovor == "Uspesno") {
        alert("Uspešno uklonjen rok!");
    }

    
}