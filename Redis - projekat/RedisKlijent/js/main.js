import { Blanket } from "./blanket.js";
import { Skripta } from "./skripta.js";
import { Beleska } from "./beleska.js";

sessionStorage.setItem("webapi","https://localhost:5001/");
var webapi = sessionStorage.getItem("webapi");

let selectGodinuPredavanja = document.querySelector(".GodinePredavanja");
let selectPredmeti = document.querySelector(".Predmeti");
let selectTip = document.querySelector(".Tip");
selectTip.querySelector("option[value=Blanketi]").selected = true;
let pageButtons = document.querySelector(".pageButtons");

let selectGodinuBlanketa = document.querySelector(".Godine");
let trenutnaGodina = new Date().getFullYear();

/*for (let br = trenutnaGodina - 200; br < trenutnaGodina + 2; br++) {

    let opt = document.createElement("option");
    opt.value = br;
    opt.innerHTML = br;
    if (br == trenutnaGodina) {
        opt.selected = true;
    }
    selectGodinuBlanketa.appendChild(opt);

}*/

var rokovi = [];

Inicijalizacija();

async function Inicijalizacija() {

    rokovi = await VratiRokove();

    await VratiPredmete(selectGodinuPredavanja.value);
    
    VratiGodineBlanketaIBlankete()

}

selectGodinuPredavanja.onchange = (ev) => {
    if (selectTip.value == "Blanketi") {
        VratiPredmeteIGodineBlanketaIBlankete();
    }
    else if (selectTip.value == "Beleske") {
        VratiPredmeteIBeleske();
    }
    else if (selectTip.value == "Skripte") {
        VratiPredmeteISkripte();
    }  
}

async function VratiPredmeteIGodineBlanketaIBlankete() {
    await VratiPredmete(selectGodinuPredavanja.value);
    await VratiGodineBlanketa(selectGodinuPredavanja.value, selectPredmeti.value);
    VratiBlankete();
}

async function VratiPredmeteISkripte() {
    await VratiPredmete(selectGodinuPredavanja.value);
    GenerisiStraniceSkripti();
}

async function VratiPredmeteIBeleske() {
    await VratiPredmete(selectGodinuPredavanja.value); 
    GenerisiStraniceBeleski();
}

async function VratiGodineBlanketaIBlankete() {
    await VratiGodineBlanketa(selectGodinuPredavanja.value, selectPredmeti.value);
    VratiBlankete();
}


selectPredmeti.onchange = (ev) => {
    if (selectTip.value == "Blanketi") {
        VratiGodineBlanketaIBlankete();
    }
    else if (selectTip.value == "Skripte") {
        GenerisiStraniceSkripti();
    }
    else if (selectTip.value == "Beleske") {
        GenerisiStraniceBeleski();
    }
}

selectTip.onchange = (ev) => {

    if (selectTip.value == "Blanketi") {
        document.querySelector(".BlanketiGodina").style.display = "block";
        document.querySelector(".Blanketi").style.display = "block";
        document.querySelector(".Beleske").style.display = "none";
        document.querySelector(".Skripte").style.display = "none";
        VratiGodineBlanketaIBlankete();

        //Brisanje dugmica
        while(pageButtons.firstChild)
            pageButtons.removeChild(pageButtons.firstChild);
        
    }
    else if (selectTip.value == "Skripte") {
        document.querySelector(".BlanketiGodina").style.display = "none";
        document.querySelector(".Blanketi").style.display = "none";
        document.querySelector(".Beleske").style.display = "none";
        document.querySelector(".Skripte").style.display = "block";
        GenerisiStraniceSkripti();
    }
    else if (selectTip.value == "Beleske") {
        document.querySelector(".BlanketiGodina").style.display = "none";
        document.querySelector(".Blanketi").style.display = "none";
        document.querySelector(".Beleske").style.display = "block";
        document.querySelector(".Skripte").style.display = "none";
        GenerisiStraniceBeleski();
    }
    
}

selectGodinuBlanketa.onchange = (ev) => {
    VratiBlankete();
}



async function VratiBlankete() {
    document.querySelector(".Blanketi").innerHTML = "";
    rokovi.forEach(r => {
        VratiBlanket(selectGodinuPredavanja.value, selectPredmeti.value, selectGodinuBlanketa.value, r);
    })
}

async function VratiPredmete(godinaPredavanja) {

    let odgovor = await FetchVratiPredmete(godinaPredavanja);

    let selectPredmeti = document.querySelector(".Predmeti");
    selectPredmeti.innerHTML = "";

    if (odgovor == false) {
        return false;
    }

    odgovor.forEach(predmet => {
        let opt = document.createElement("option");
        opt.value = predmet;
        opt.innerHTML = predmet;
        selectPredmeti.appendChild(opt);
    })

}

async function FetchVratiPredmete(godinaPredavanja) {

    let odgovor = await fetch(webapi + "Student/VratiPredmete/" + godinaPredavanja, {
        method: 'GET'
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.json().catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    return odgovor;

}

async function VratiBrojSkripti(){

    if (selectGodinuPredavanja.value == "" || selectPredmeti.value == "") {
       return false;
   }

   let brojSkripti = await FetchVratiBrojSkripti(selectGodinuPredavanja.value, selectPredmeti.value);

   if (brojSkripti == false) {
       return false;
   }

   return brojSkripti;
}

async function FetchVratiBrojSkripti(godinaPredavanja, predmet){
    let odgovor = await fetch(webapi + "Student/VratiBrojSkripti/" + godinaPredavanja + "/" + predmet, {
        method: 'GET'
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.json().catch(reason => false);

    if (odgovor == false) {
        return false;
    }
    
    return odgovor;
}

async function VratiBrojBeleski(){

    if (selectGodinuPredavanja.value == "" || selectPredmeti.value == "") {
       return false;
   }

   let brojBeleski = await FetchVratiBrojBeleski(selectGodinuPredavanja.value, selectPredmeti.value);

   if (brojBeleski == false) {
       return false;
   }

   return brojBeleski;
}

async function FetchVratiBrojBeleski(godinaPredavanja, predmet){
    let odgovor = await fetch(webapi + "Student/VratiBrojBeleski/" + godinaPredavanja + "/" + predmet, {
        method: 'GET'
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.json().catch(reason => false);

    if (odgovor == false) {
        return false;
    }
    
    return odgovor;
}

async function VratiSkriptePredmeta(){

    if (selectGodinuPredavanja.value == "" || selectPredmeti.value == "") {
       return false;
   }

   let skripte = await FetchVratiSkriptePredmeta(selectGodinuPredavanja.value, selectPredmeti.value);

   if (skripte == false) {
       return false;
   }

   let kontejner = document.querySelector(".Blanketi");

   skripte.forEach(e => {
       e.PrikazSkripte(kontejner)
   })

}

async function FetchVratiSkriptePredmeta(godinaPredavanja, predmet){
    let odgovor = await fetch(webapi + "Student/VratiSkripte/" + godinaPredavanja + "/" + predmet, {
        method: 'GET'
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.json().catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    let skripte = [];

    odgovor.forEach(skripta => {

        let putanjeDoFajlova = [];
        skripta.putanjeDoFajlova.forEach(e => {
            putanjeDoFajlova.push(e);
        })

        let fajlovi = [];
        skripta.fajlovi.forEach(e => {
            fajlovi.push(e);
        })

        skripte.push(new Skripta(skripta.naziv, skripta.godinaPredavanja, skripta.predmet,  fajlovi, putanjeDoFajlova, skripta.autor, skripta.ticksDatum));
    })

    return skripte;
}

async function Vrati5SkriptePredmeta(pozicija){

    if (selectGodinuPredavanja.value == "" || selectPredmeti.value == "") {
       return false;
   }

   let skripte = await FetchVrati5SkriptePredmeta(selectGodinuPredavanja.value, selectPredmeti.value, pozicija);

   if (skripte == false) {
       return false;
   }

   let kontejner = document.querySelector(".Skripte");

   skripte.forEach(e => {
       e.PrikazSkripte(kontejner)
   })

}

async function FetchVrati5SkriptePredmeta(godinaPredavanja, predmet, pozicija){
    let odgovor = await fetch(webapi + "Student/Vrati5Skripte/" + godinaPredavanja + "/" + predmet + "/" + pozicija, {
        method: 'GET'
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.json().catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    let skripte = [];

    odgovor.forEach(skripta => {

        let putanjeDoFajlova = [];
        skripta.putanjeDoFajlova.forEach(e => {
            putanjeDoFajlova.push(e);
        })

        let fajlovi = [];
        skripta.fajlovi.forEach(e => {
            fajlovi.push(e);
        })

        skripte.push(new Skripta(skripta.naziv, skripta.godinaPredavanja, skripta.predmet,  fajlovi, putanjeDoFajlova, skripta.autor, skripta.ticksDatumString));
    })

    return skripte;
}

async function GenerisiStraniceSkripti() {

    //Izbiri postojece dugmice
    while(pageButtons.firstChild)
        pageButtons.removeChild(pageButtons.firstChild);

    let brojStranica = await VratiBrojSkripti();
    if (brojStranica % 5 == 0) {
        brojStranica--;
    }
    brojStranica = brojStranica / 5;
    brojStranica = Math.floor(brojStranica);

    for(let i=0; i <= brojStranica; i++){
        let dugmeStranice = document.createElement("button");
        dugmeStranice.innerHTML = i+1;
        dugmeStranice.classList.add("btn","DugmeStranice");
        dugmeStranice.classList.add("btn-outline-secondary");
        dugmeStranice.onclick = (ev) => {

            document.querySelectorAll(".DugmeStranice").forEach(d => {
                d.classList.remove("bg-dark","text-light");
            })

            dugmeStranice.classList.add("bg-dark","text-light");

            let skripte = document.querySelector(".Skripte")

            // Brise postojeci sadrzaj na stanici
            while(skripte.firstChild)
                skripte.removeChild(skripte.firstChild);

            Vrati5SkriptePredmeta((i)*5);
        }
        pageButtons.appendChild(dugmeStranice);
        if(i === 0)
            dugmeStranice.click();

    }

}

async function Vrati5BeleskePredmeta(pozicija){

    if (selectGodinuPredavanja.value == "" || selectPredmeti.value == "") {
       return false;
   }

   let beleske = await FetchVrati5BeleskePredmeta(selectGodinuPredavanja.value, selectPredmeti.value, pozicija);


   if (beleske == false) {
       return false;
   }

   let kontejner = document.querySelector(".Beleske");

   beleske.forEach(e => {
       e.PrikazBeleske(kontejner)
   })

}

async function FetchVrati5BeleskePredmeta(godinaPredavanja, predmet, pozicija){
    let odgovor = await fetch(webapi + "Student/Vrati5Beleske/" + godinaPredavanja + "/" + predmet + "/" + pozicija, {
        method: 'GET'
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.json().catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    let beleske = [];

    odgovor.forEach(beleska => {

        let putanjeDoFajlova = [];
        beleska.putanjeDoFajlova.forEach(e => {
            putanjeDoFajlova.push(e);
        })

        let fajlovi = [];
        beleska.fajlovi.forEach(e => {
            fajlovi.push(e);
        })

        beleske.push(new Beleska(beleska.naziv, beleska.godinaPredavanja, beleska.predmet,  fajlovi, putanjeDoFajlova, beleska.autor, beleska.ticksDatumString));
    })

    return beleske;
}

async function GenerisiStraniceBeleski() {

    //Izbiri postojece dugmice
    while(pageButtons.firstChild)
        pageButtons.removeChild(pageButtons.firstChild);

    let brojStranica = await VratiBrojBeleski();
    if (brojStranica % 5 == 0) {
        brojStranica--;
    }
    brojStranica = brojStranica / 5;
    
    brojStranica = Math.floor(brojStranica);

    for(let i=0; i <= brojStranica; i++){
        let dugmeStranice = document.createElement("button");
        dugmeStranice.innerHTML = i+1;
        dugmeStranice.classList.add("btn","DugmeStranice");
        dugmeStranice.classList.add("btn-outline-secondary");
        dugmeStranice.onclick = (ev) => {

            document.querySelectorAll(".DugmeStranice").forEach(d => {
                d.classList.remove("bg-dark","text-light");
            })
            dugmeStranice.classList.add("bg-dark","text-light");

            let beleske = document.querySelector(".Beleske");

            // Brise postojeci sadrzaj na stanici
            while(beleske.firstChild)
                beleske.removeChild(beleske.firstChild);

            Vrati5BeleskePredmeta((i)*5);
        }
        pageButtons.appendChild(dugmeStranice);
        if(i === 0)
            dugmeStranice.click();

    }

}


async function VratiGodineBlanketa(godinaPredavanja, predmet) {

    let selectGodinaBlanketa = document.querySelector(".Godine");
    selectGodinaBlanketa.innerHTML = "";

    if (predmet == "") {
        return false;
    }

    let odgovor = await FetchVratiGodineBlanketa(godinaPredavanja, predmet);

    if (odgovor == false) {
        return false;
    }

    odgovor.forEach(god => {
        let opt = document.createElement("option");
        opt.value = god;
        opt.innerHTML = god;
        selectGodinaBlanketa.appendChild(opt);
    })

}

async function FetchVratiGodineBlanketa(godinaPredavanja, predmet) {

    let odgovor = await fetch(webapi + "Student/VratiGodineBlanketa/" + godinaPredavanja + "/" + predmet, {
        method: 'GET'
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.json().catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    return odgovor;

}



async function VratiBlanket(godinaPredavanja, predmet, godina, rok) {

    if (godinaPredavanja == "" || predmet == "" || godina == "" || rok == "") {
        return false;
    }

    let blanket = await FetchVratiBlanket(godinaPredavanja, predmet, godina, rok);

    if (blanket == false) {
        return false;
    }

    let kontejner = document.querySelector(".Blanketi");
    blanket.PrikazBlanketa(kontejner);

}

async function FetchVratiBlanket(godinaPredavanja, predmet, godina, rok) {

    let odgovor = await fetch(webapi + "Student/VratiBlanket/" +godinaPredavanja+"/"+predmet + "/" + godina + "/" + rok, {
        method: 'GET',
        headers: {

        }
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.json().catch(reason => false);

    if (odgovor == false || odgovor == null) {
        return false;
    }

    let b = odgovor;


    return new Blanket(b.godinaPredavanja, b.predmet, b.godina, b.rok, b.fajlovi, b.putanjeDoFajlova);



    
}

async function VratiRokove() {

    let odgovor = await FetchVratiRokove();

    if (odgovor == false) {
        return false;
    }

    odgovor.forEach(r => {
        rokovi.push(r);
    })

    return odgovor;

}

async function FetchVratiRokove() {

    let odgovor = await fetch(webapi + "Student/VratiRokove", {
        method: 'GET'
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = odgovor.json().catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    return odgovor;

}


let prijaviSeDugme = document.querySelector(".DugmePrijaviSe");
prijaviSeDugme.onclick = (ev) => {
    PrijaviSe();
}


async function PrijaviSe() {

    let fp = document.querySelector(".FormaZaPrijavljivanje");
        fp.querySelectorAll(".invalid-feedback").forEach(p => {
            p.style.display = "none";
        })

    let korisnickoIme = fp.querySelector("input[name=KorisnickoIme]").value;
    let sifra = fp.querySelector("input[name=Sifra]").value;

    if(korisnickoIme == "" || sifra == ""){

        document.querySelector(".PrijavljivanjePopuniteSvaPolja").style.display = "block";
        return;

    }


    PrijavaFetch(korisnickoIme, sifra, fp);    

}

async function PrijavaFetch(korisnickoIme, sifra, fp) {

    let odgovor = await fetch(webapi + "Administrator/Prijava", {
        method: "POST",
        headers:{
            "Content-Type" : "application/json"
        },
        body: JSON.stringify({
            korisnickoIme : korisnickoIme,
            sifra : sifra
        })
    }).catch(reason => {

        
        return false;

    })

    if (odgovor == false){
        fp.querySelector(".PrijavljivanjeDosloJeDoGreske").style.display = "block";
        return false;
    }
        
    odgovor = await odgovor.text().catch(response => {
        return false;
    });

    if (odgovor == false){
        fp.querySelector(".PrijavljivanjeDosloJeDoGreske").style.display = "block";
        return false;
    }

    if (odgovor == "Netacan username") {
        document.querySelector(".PrijavljivanjeKorisnickoImeFeedback").style.display = "block";
    }
    else {
        if (odgovor == "Netacna sifra") {
            document.querySelector(".PrijavljivanjeSifraFeedback").style.display = "block";
        }
        else {

            localStorage.setItem("token",odgovor);
            window.location.reload();
            window.open("administrator.html", '_blank','noopener noreferrer');

        }
    }

}

let odjaviSe = document.querySelector(".OdjaviSe");
if (localStorage.getItem("token") == null) {
    odjaviSe.style.display = "none";
}
odjaviSe.onclick = (ev) => {
    localStorage.removeItem("token");
    window.location.reload();
}