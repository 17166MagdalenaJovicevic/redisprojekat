sessionStorage.setItem("webapi","https://localhost:5001/");
var webapi = sessionStorage.getItem("webapi");

document.querySelector(".fileInput").innerHTML = '<input type="file" accept="text/plain,.pdf,.doc,.docx,.png,.jpg,.jpeg" class="form-control" name="Fajlovi" style="display: none;" multiple>';


let dugmeIzaberiFajlove = document.querySelector(".IzaberiFajlove");
dugmeIzaberiFajlove.onclick = (ev) => {
    document.querySelector("input[name=Fajlovi]").click();
}

document.querySelector("input[name=Fajlovi]").onchange = (ev) => {
    document.querySelector(".OdabraniFajlovi").innerHTML = "";
    for(let i = 0; i < document.querySelector("input[name=Fajlovi]").files.length; i++) {
        document.querySelector(".OdabraniFajlovi").innerHTML += document.querySelector("input[name=Fajlovi]").files[i].name + "; ";
    }
        

}

let selectGodinuPredavanja = document.querySelector(".GodinePredavanja");
let selectPredmeti = document.querySelector(".Predmeti");

Inicijalizacija();

async function Inicijalizacija() {

    await VratiPredmete(selectGodinuPredavanja.value);

}

selectGodinuPredavanja.onchange = (ev) => {
    VratiPredmete(selectGodinuPredavanja.value);
}

async function VratiPredmete(godinaPredavanja) {

    let odgovor = await FetchVratiPredmete(godinaPredavanja);

    let selectPredmeti = document.querySelector(".Predmeti");
    selectPredmeti.innerHTML = "";

    if (odgovor == false) {
        return false;
    }

    odgovor.forEach(predmet => {
        let opt = document.createElement("option");
        opt.value = predmet;
        opt.innerHTML = predmet;
        selectPredmeti.appendChild(opt);
    })

}

async function FetchVratiPredmete(godinaPredavanja) {

    let odgovor = await fetch(webapi + "Student/VratiPredmete/" + godinaPredavanja, {
        method: 'GET'
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.json().catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    return odgovor;

}

let dugmePosaljiBeleske = document.querySelector(".DugmeDodajBeleske");
dugmePosaljiBeleske.onclick = (ev) => {
    PosaljiBeleske();
}


async function PosaljiBeleske() {

    document.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    document.querySelector(".Uspesno").style.display = "none";

    let fajloviPom = document.querySelector("input[name=Fajlovi]").files;
    let fajlovi = new FormData();

    let naziv = document.querySelector("input[name=Naziv]").value;
    if (naziv == "") {
        document.querySelector(".UnesiteNaziv").style.display = "block";
        return false;
    }

    let autor = document.querySelector("input[name=Autor]").value;
    if (autor == "") {
        document.querySelector(".UnesiteAutora").style.display = "block";
        return false;
    }

    if (fajloviPom.length == 0) {
        document.querySelector(".NisteIzabraliFajlove").style.display = "block";
        return;
    }

    for(let i = 0; i < fajloviPom.length; i++) {
        fajlovi.append('fajlovi',fajloviPom[i]);
    }

    await FetchPosaljiBeleske(selectGodinuPredavanja.value, selectPredmeti.value, naziv, autor, fajlovi);

}

async function FetchPosaljiBeleske(godinaPredavanja, predmet, naziv, autor, fajlovi) {

    let odgovor = await fetch(webapi+"Student/DodajBeleske/"+godinaPredavanja+"/"+predmet + "/" + naziv + "/" + autor, {
        method:"POST",
        headers:{
            "Accept" : "text/plain,.pdf,.doc,.docx,image/png,image/jpg"
        },
        body: fajlovi
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        return false;
    }

    odgovor = await odgovor.text().catch(reason => false);

    if (odgovor == false){
        return false;
    }

    if (odgovor == "Uspesno") {
        document.querySelector(".Uspesno").style.display = "block";
        
    } else if (odgovor == "Vec 50 beleski na cekanju") {
        document.querySelector(".VecDesetPoslato").style.display = "block";
    }

}