sessionStorage.setItem("webapi","https://localhost:5001/");
var webapi = sessionStorage.getItem("webapi");

document.querySelector(".fileInput").innerHTML = '<input type="file" accept="text/plain,.pdf,.doc,.docx,.png,.jpg,.jpeg" class="form-control" name="Fajlovi" style="display: none;" multiple>';


let dugmeIzaberiFajlove = document.querySelector(".IzaberiFajlove");
dugmeIzaberiFajlove.onclick = (ev) => {
    document.querySelector("input[name=Fajlovi]").click();
}

document.querySelector("input[name=Fajlovi]").onchange = (ev) => {
    document.querySelector(".OdabraniFajlovi").innerHTML = "";
    for(let i = 0; i < document.querySelector("input[name=Fajlovi]").files.length; i++) {
        document.querySelector(".OdabraniFajlovi").innerHTML += document.querySelector("input[name=Fajlovi]").files[i].name + "; ";
    }
        

}

let selectGodinu = document.querySelector(".Godine");
let trenutnaGodina = new Date().getFullYear();

for (let br = trenutnaGodina - 100; br < trenutnaGodina + 2; br++) {

    let opt = document.createElement("option");
    opt.value = br;
    opt.innerHTML = br;
    if (br == trenutnaGodina) {
        opt.selected = true;
    }
    selectGodinu.appendChild(opt);

}

let selectGodinuPredavanja = document.querySelector(".GodinePredavanja");
let selectRok = document.querySelector(".Rokovi");
let selectPredmeti = document.querySelector(".Predmeti");

Inicijalizacija();

async function Inicijalizacija() {

    await VratiRokove();
    await VratiPredmete(selectGodinuPredavanja.value);

}

selectGodinuPredavanja.onchange = (ev) => {
    VratiPredmete(selectGodinuPredavanja.value);
}


async function VratiRokove() {

    let odgovor = await FetchVratiRokove();

    if (odgovor == false) {
        return false;
    }

    let selectRok = document.querySelector(".Rokovi");

    odgovor.forEach(rok => {
        let opt = document.createElement("option");
        opt.value = rok;
        opt.innerHTML = rok;
        selectRok.appendChild(opt);
    })

}

async function FetchVratiRokove() {

    let odgovor = await fetch(webapi + "Student/VratiRokove", {
        method: 'GET'
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = odgovor.json().catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    return odgovor;

}

async function VratiPredmete(godinaPredavanja) {

    let odgovor = await FetchVratiPredmete(godinaPredavanja);

    let selectPredmeti = document.querySelector(".Predmeti");
    selectPredmeti.innerHTML = "";

    if (odgovor == false) {
        return false;
    }

    odgovor.forEach(predmet => {
        let opt = document.createElement("option");
        opt.value = predmet;
        opt.innerHTML = predmet;
        selectPredmeti.appendChild(opt);
    })

}

async function FetchVratiPredmete(godinaPredavanja) {

    let odgovor = await fetch(webapi + "Student/VratiPredmete/" + godinaPredavanja, {
        method: 'GET'
    }).catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    odgovor = await odgovor.json().catch(reason => false);

    if (odgovor == false) {
        return false;
    }

    return odgovor;

}


let dugmePosaljiBlanket = document.querySelector(".DugmeDodajBlanket");
dugmePosaljiBlanket.onclick = (ev) => {
    PosaljiBlanket();
}


async function PosaljiBlanket() {

    document.querySelectorAll(".invalid-feedback").forEach(p => {
        p.style.display = "none";
    })

    document.querySelector(".Uspesno").style.display = "none";

    let fajloviPom = document.querySelector("input[name=Fajlovi]").files;
    let fajlovi = new FormData();

    if (fajloviPom.length == 0) {
        document.querySelector(".NisteIzabraliFajlove").style.display = "block";
        return;
    }

    for(let i = 0; i < fajloviPom.length; i++) {
        fajlovi.append('fajlovi',fajloviPom[i]);
    }

    await FetchPosaljiBlanket(selectGodinuPredavanja.value, selectPredmeti.value, selectGodinu.value, selectRok.value, fajlovi);

}

async function FetchPosaljiBlanket(godinaPredavanja, predmet, godina, rok, fajlovi) {

    let odgovor = await fetch(webapi+"Student/DodajBlanket/"+godinaPredavanja+"/"+predmet + "/" + godina + "/" + rok, {
        method:"POST",
        headers:{
            "Accept" : "text/plain,.pdf,.doc,.docx,image/png,image/jpg"
        },
        body: fajlovi
    }).catch(reason=>{
        return false;
    })

    if (odgovor == false){
        return false;
    }

    odgovor = await odgovor.text().catch(reason => false);

    if (odgovor == false){
        return false;
    }

    if (odgovor == "Uspesno") {
        document.querySelector(".Uspesno").style.display = "block";
        
    } else if (odgovor == "Vec 10 blanketa na cekanju") {
        document.querySelector(".VecDesetPoslato").style.display = "block";
    }

}