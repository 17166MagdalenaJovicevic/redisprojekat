using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RedisServer.Models;
using ServiceStack.Redis;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Security.Cryptography;

namespace RedisServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StudentController : ControllerBase
    {
        // private readonly IRedisClientsManager _manager;
        private readonly RedisClient client;

        private readonly List<string> godine;

        public StudentController()//IRedisClientsManager manager)
        {
            //_manager = manager;
            client = new RedisClient("127.0.0.1:6379");
            client.ConfigSet("appendonly",Encoding.UTF8.GetBytes("yes"));   // da svaku operaciju lepi u fajl (log)

            godine = new List<string>();
            godine.Add("I");
            godine.Add("II");
            godine.Add("III");
            godine.Add("IV");
            godine.Add("V");
            godine.Add("Master");

            if (client.Get("nepotvrdjenebeleskebroj") == null) {
                client.Set("nepotvrdjenebeleskebroj", 0);
            }

            if(client.Get("nepotvrdjeneskriptebroj") == null){
                client.Set("nepotvrdjeneskriptebroj", 0);
            }


            
        }

        

        [HttpGet]
        [Route("VratiPredmete/{GodinaPredavanja}")]   // student
        public List<string> VratiPredmete([FromRoute] string GodinaPredavanja) {

            List<string> predmeti = new List<string>();

            if (!godine.Contains(GodinaPredavanja)) {
                return predmeti;
            }

            var kljuc = GodinaPredavanja;

            byte[][] predmetiBytes = client.SMembers(kljuc);

            

            foreach(var p in predmetiBytes) {
                predmeti.Add(Encoding.UTF8.GetString(p));
            }

            return predmeti;

        }

        [HttpGet]
        [Route("VratiRokove")]   // student
        public List<string> VratiRokove() {

            List<string> rokovi = new List<string>();

            var kljuc = "rok";

            byte[][] rokoviBytes = client.SMembers(kljuc);

            

            foreach(var p in rokoviBytes) {
                rokovi.Add(Encoding.UTF8.GetString(p));
            }

            return rokovi;

        }

        


        [HttpPost]
        [Route("DodajBlanket/{GodinaPredavanja}/{Predmet}/{Godina}/{Rok}")]    // student
        public string DodajBlanket([FromRoute] string GodinaPredavanja, [FromRoute] string Predmet, [FromRoute] string Godina, [FromRoute] string Rok, [FromForm] List<IFormFile> fajlovi) {

            // prvo se dodaje u nepotvrdjeno kad student dodaje

            if (!godine.Contains(GodinaPredavanja)) {
                return "Nevalidna godina predavanja";
            }

            if (Int32.Parse(Godina) > DateTime.UtcNow.AddYears(1).Year && Int32.Parse(Godina) < DateTime.UtcNow.AddYears(-100).Year) {
                return "Nevalidna godina";
            }

            var predmetBytes = Encoding.UTF8.GetBytes(Predmet);

            if (client.SIsMember(GodinaPredavanja, predmetBytes) == 0) {
                return "Nevalidan predmet";
            }

            var rokBytes = Encoding.UTF8.GetBytes(Rok);

            if (client.SIsMember("rok", rokBytes) == 0) {
                return "Nevalidan rok";
            }

            // ukoliko postoji u  nepotvrdjeno vec, samo da se doda putanja jos koliko fajlova je poslato
            string kljuc = "nepotvrdjeno:blanket:" + GodinaPredavanja + ":" + Predmet.Replace(" ","") + ":" + Godina + ":" + Rok;
            var rez = client.Get<Blanket>(kljuc);
            Blanket blanket = new Blanket();


            if (rez != null) {
                //blanket.PutanjeDoFajlova = JsonSerializer.Deserialize<List<string>>(blanket.PutanjeDoFajlovaString);
                blanket = rez;
                if (blanket.PutanjeDoFajlova.Count + fajlovi.Count > 10) {
                    return "Vec 10 blanketa na cekanju";
                }
                if (fajlovi.Count > 0) {
                    foreach(var fajl in fajlovi) {
                        string putanja = "../../db/Nepotvrdjeno/Blanketi/" + Blanket.NapraviNazivFajla(GodinaPredavanja, Predmet, Godina, Rok, blanket.PutanjeDoFajlova.Count, fajl.FileName);
                        var podatak = new MemoryStream();
                        fajl.CopyTo(podatak);
                        System.IO.File.WriteAllBytes(putanja, podatak.ToArray());
                        blanket.PutanjeDoFajlova.Add(putanja);
                    }
                }
                
            }
            else {
                blanket.Predmet = Predmet;
                blanket.Rok = Rok;
                blanket.GodinaPredavanja = GodinaPredavanja;
                blanket.Godina = Godina;
                blanket.PutanjeDoFajlova = new List<string>();
                if (fajlovi.Count > 0) {
                    foreach(var fajl in fajlovi) {
                        string putanja = "../../db/Nepotvrdjeno/Blanketi/" + Blanket.NapraviNazivFajla(GodinaPredavanja, Predmet, Godina, Rok, blanket.PutanjeDoFajlova.Count, fajl.FileName);
                        var podatak = new MemoryStream();
                        fajl.CopyTo(podatak);
                        System.IO.File.WriteAllBytes(putanja, podatak.ToArray());
                        blanket.PutanjeDoFajlova.Add(putanja);
                    }
                }
            }

            //blanket.PutanjeDoFajlovaString = JsonSerializer.Serialize(blanket.PutanjeDoFajlova);

            client.Set<Blanket>(kljuc,blanket);

            return "Uspesno";


        }


        [HttpPost]
        [Route("DodajBeleske/{GodinaPredavanja}/{Predmet}/{Naziv}/{Autor}")]    // student
        public string DodajBeleske([FromRoute] string GodinaPredavanja, [FromRoute] string Predmet, [FromRoute] string Naziv, [FromRoute] string Autor, [FromForm] List<IFormFile> fajlovi) {

            // prvo se dodaje u nepotvrdjeno kad student dodaje

            if (!godine.Contains(GodinaPredavanja)) {
                return "Nevalidna godina predavanja";
            }

            var predmetBytes = Encoding.UTF8.GetBytes(Predmet);

            if (client.SIsMember(GodinaPredavanja, predmetBytes) == 0) {
                return "Nevalidan predmet";
            }


            // ukoliko postoji u  nepotvrdjeno vec, samo da se doda putanja jos koliko fajlova je poslato
            string kljuc = "nepotvrdjeno:beleske:" + GodinaPredavanja + ":" + Predmet.Replace(" ",""); // dodaje se u sortirani skup
            Beleska beleska = new Beleska();

            beleska.Predmet = Predmet;
            beleska.Naziv = Naziv;
            beleska.Autor = Autor;
            beleska.GodinaPredavanja = GodinaPredavanja;
            beleska.PutanjeDoFajlova = new List<string>();

            string br = Encoding.UTF8.GetString(client.Get("nepotvrdjenebeleskebroj"));
            long brojBeleski = Int64.Parse(br);
            long poRedu = brojBeleski;

            if (fajlovi.Count > 0 && brojBeleski + fajlovi.Count < 50) {
                foreach(var fajl in fajlovi) {
                    string putanja = "../../db/Nepotvrdjeno/Beleske/" + poRedu + fajl.FileName;
                    var podatak = new MemoryStream();
                    fajl.CopyTo(podatak);
                    System.IO.File.WriteAllBytes(putanja, podatak.ToArray());
                    poRedu = client.Incr("nepotvrdjenebeleskebroj");
                    beleska.PutanjeDoFajlova.Add(putanja);
                }
            }
            else {
                return "Vec 50 beleski na cekanju";
            }
            beleska.TicksDatum = DateTime.UtcNow.Ticks;

            byte[] beleskaBytes = JsonSerializer.SerializeToUtf8Bytes(beleska);

            client.ZAdd(kljuc, beleska.TicksDatum, beleskaBytes);

            return "Uspesno";


        }

        [HttpGet]
        [Route("VratiBrojBeleski/{Godina}/{Predmet}")]
        public long VratiBrojBeleski([FromRoute] string Godina, [FromRoute] string Predmet) {

            long brojBeleski = client.ZCard("beleske:" + Godina + ":" + Predmet.Replace(" ",""));
            return brojBeleski;
            
        }

        [HttpGet]
        [Route("VratiBeleske/{Godina}/{Predmet}")]
        public List<Beleska> VratiBeleske([FromRoute] string Godina, [FromRoute] string Predmet) {

            var beleske = client.ZRange("beleske:" + Godina + ":" + Predmet,0,-1);
            var beleskeReturn = new List<Beleska>();

            for(int i =0 ; i<beleske.GetLength(0); i++)
            {
                var beleska = JsonSerializer.Deserialize<Beleska>(beleske[i]);
                beleskeReturn.Add(beleska);
            }

            foreach(var beleska in beleskeReturn) {
                beleska.Fajlovi = new List<byte[]>();
                for(int i = 0; i < beleska.PutanjeDoFajlova.Count; i++) 
                {
                    beleska.Fajlovi.Add(System.IO.File.ReadAllBytes(beleska.PutanjeDoFajlova[i]));
                    beleska.PutanjeDoFajlova[i] = beleska.PutanjeDoFajlova[i].Split("/").Last();
                }
            }

            return beleskeReturn;
        }

        [HttpGet]
        [Route("Vrati5Beleske/{Godina}/{Predmet}/{Polozaj}")]
        public List<Beleska> Vrati5Beleske([FromRoute] string Godina, [FromRoute] string Predmet, [FromRoute] int Polozaj) {

            // Generisanje zavrsnog polozaja
            long brojBeleski = client.ZCard("beleske:" + Godina + ":" + Predmet.Replace(" ",""));
            Polozaj = - Polozaj - 1;
            int PolozajZavrsetka = Polozaj - 4;
            if(PolozajZavrsetka <= -brojBeleski-1)
                PolozajZavrsetka = 0;

            var beleske = client.ZRange("beleske:" + Godina + ":" + Predmet.Replace(" ",""), PolozajZavrsetka, Polozaj);
            var beleskeReturn = new List<Beleska>();

            for(int i =0 ; i<beleske.GetLength(0); i++)
            {
                var beleska = JsonSerializer.Deserialize<Beleska>(beleske[i]);
                beleska.TicksDatumString = beleska.TicksDatum.ToString();
                beleskeReturn.Add(beleska);
            }

            foreach(var beleska in beleskeReturn) {
                beleska.Fajlovi = new List<byte[]>();
                for(int i = 0; i < beleska.PutanjeDoFajlova.Count; i++) 
                {
                    beleska.Fajlovi.Add(System.IO.File.ReadAllBytes(beleska.PutanjeDoFajlova[i]));
                    beleska.PutanjeDoFajlova[i] = beleska.PutanjeDoFajlova[i].Split("/").Last();
                }
            }

            beleskeReturn.Reverse();

            return beleskeReturn;
        }


        [HttpGet]
        [Route("VratiGodineBlanketa/{GodinaPredavanja}/{Predmet}")]
        public List<string> VratiGodineBlanketa([FromRoute] string GodinaPredavanja, [FromRoute] string Predmet) {

            byte[][] godine1 = client.ZRange("blanket:" + GodinaPredavanja + ":" + Predmet.Replace(" ",""),0,-1);
            List<string> godine = new List<string>();
            if (godine1.Count() > 0) {
                foreach(var godina in godine1) {
                    godine.Add(Encoding.UTF8.GetString(godina));
                }
                
            }
            godine.Reverse();
            return godine;

        }

        [HttpGet]
        [Route("VratiBlanket/{GodinaPredavanja}/{Predmet}/{Godina}/{Rok}")]
        public Blanket VratiBlankete([FromRoute] string GodinaPredavanja, [FromRoute] string Predmet, [FromRoute] string Godina, [FromRoute] string Rok) {

            string key = "blanket:" + GodinaPredavanja + ":" + Predmet.Replace(" ","") + ":" + Godina + ":" + Rok;
            Blanket blanket = client.Get<Blanket>(key);

            if (blanket == null) {
                return null;
            }


            blanket.Fajlovi = new List<byte[]>();
            for(int i = 0; i < blanket.PutanjeDoFajlova.Count; i++) {
                blanket.Fajlovi.Add(System.IO.File.ReadAllBytes(blanket.PutanjeDoFajlova[i]));
                blanket.PutanjeDoFajlova[i] = blanket.PutanjeDoFajlova[i].Split("/").Last();
            }


            return blanket;

        }

        [HttpPost]
        [Route("DodajSkripte/{GodinaPredavanja}/{Predmet}/{Naziv}/{Autor}")]    // student
        public string DodajSkripte([FromRoute] string GodinaPredavanja, [FromRoute] string Predmet, [FromRoute] string Naziv, [FromRoute] string Autor, [FromForm] List<IFormFile> fajlovi) {

            // prvo se dodaje u nepotvrdjeno kad student dodaje

            if (!godine.Contains(GodinaPredavanja)) {
                return "Nevalidna godina predavanja";
            }

            var predmetBytes = Encoding.UTF8.GetBytes(Predmet);

            if (client.SIsMember(GodinaPredavanja, predmetBytes) == 0) {
                return "Nevalidan predmet";
            }


            // ukoliko postoji u  nepotvrdjeno vec, samo da se doda putanja jos koliko fajlova je poslato
            string kljuc = "nepotvrdjeno:skripte:" + GodinaPredavanja + ":" + Predmet.Replace(" ",""); // dodaje se u sortirani skup
            Skripta skripta = new Skripta();

            skripta.Predmet = Predmet;
            skripta.Naziv = Naziv;
            skripta.Autor = Autor;
            skripta.GodinaPredavanja = GodinaPredavanja;
            skripta.PutanjeDoFajlova = new List<string>();

            string br = Encoding.UTF8.GetString(client.Get("nepotvrdjeneskriptebroj"));
            long brojBeleski = Int64.Parse(br);
            long poRedu = brojBeleski;

            if (fajlovi.Count > 0 && brojBeleski + fajlovi.Count < 50) {
                foreach(var fajl in fajlovi) {
                    string putanja = "../../db/Nepotvrdjeno/Skripte/" + poRedu + fajl.FileName;
                    var podatak = new MemoryStream();
                    fajl.CopyTo(podatak);
                    System.IO.File.WriteAllBytes(putanja, podatak.ToArray());
                    poRedu = client.Incr("nepotvrdjeneskriptebroj");
                    skripta.PutanjeDoFajlova.Add(putanja);
                }
            }
            else {
                return "Vec 50 beleski na cekanju";
            }

            skripta.TicksDatum = DateTime.UtcNow.Ticks;

            byte[] skriptaBytes = JsonSerializer.SerializeToUtf8Bytes(skripta);

            client.ZAdd(kljuc, skripta.TicksDatum, skriptaBytes);

            return "Uspesno";

        }
        [HttpGet]
        [Route("VratiBrojSkripti/{Godina}/{Predmet}")]
        public long VratiBrojSkripti([FromRoute] string Godina, [FromRoute] string Predmet) {

            long brojSkripti = client.ZCard("skripte:" + Godina + ":" + Predmet.Replace(" ",""));
            return brojSkripti;
            
        }

        [HttpGet]
        [Route("VratiSkripte/{Godina}/{Predmet}")]
        public List<Skripta> VratiSkripte([FromRoute] string Godina, [FromRoute] string Predmet) {

            var skripte = client.ZRange("skripte:" + Godina + ":" + Predmet,0,-1);
            var skripteReturn = new List<Skripta>();

            for(int i =0 ; i<skripte.GetLength(0); i++)
            {
                var skripta = JsonSerializer.Deserialize<Skripta>(skripte[i]);
                skripteReturn.Add(skripta);
            }

            foreach(var skripta in skripteReturn) {
                skripta.Fajlovi = new List<byte[]>();
                for(int i = 0; i < skripta.PutanjeDoFajlova.Count; i++) 
                {
                    skripta.Fajlovi.Add(System.IO.File.ReadAllBytes(skripta.PutanjeDoFajlova[i]));
                    skripta.PutanjeDoFajlova[i] = skripta.PutanjeDoFajlova[i].Split("/").Last();
                }
            }

            return skripteReturn;
        }

        [HttpGet]
        [Route("Vrati5Skripte/{Godina}/{Predmet}/{Polozaj}")]
        public List<Skripta> Vrati5Skripte([FromRoute] string Godina, [FromRoute] string Predmet, [FromRoute] int Polozaj) {

            // Generisanje zavrsnog polozaja
            long brojSkripti = client.ZCard("skripte:" + Godina + ":" + Predmet.Replace(" ",""));
            Polozaj = - Polozaj - 1;
            int PolozajZavrsetka = Polozaj - 4;
            if(PolozajZavrsetka <= -brojSkripti-1)
                PolozajZavrsetka = 0;

            var skripte = client.ZRange("skripte:" + Godina + ":" + Predmet.Replace(" ",""), PolozajZavrsetka, Polozaj);
            var skripteReturn = new List<Skripta>();

            for(int i =0 ; i<skripte.GetLength(0); i++)
            {
                var skripta = JsonSerializer.Deserialize<Skripta>(skripte[i]);
                skripta.TicksDatumString = skripta.TicksDatum.ToString();
                skripteReturn.Add(skripta);
            }

            foreach(var skripta in skripteReturn) {
                skripta.Fajlovi = new List<byte[]>();
                for(int i = 0; i < skripta.PutanjeDoFajlova.Count; i++) 
                {
                    skripta.Fajlovi.Add(System.IO.File.ReadAllBytes(skripta.PutanjeDoFajlova[i]));
                    skripta.PutanjeDoFajlova[i] = skripta.PutanjeDoFajlova[i].Split("/").Last();
                }
            }

            skripteReturn.Reverse();

            return skripteReturn;
        }


        
        

    }

}