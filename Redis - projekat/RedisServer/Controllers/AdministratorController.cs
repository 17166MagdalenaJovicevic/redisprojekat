using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RedisServer.Models;
using ServiceStack.Redis;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Security.Cryptography;
using Microsoft.Extensions.Primitives;

[ApiController]
    [Route("[controller]")]
    public class AdministratorController : ControllerBase
    {

        private readonly RedisClient client;

        private readonly List<string> godine;

        public AdministratorController()
        {

            client = new RedisClient("127.0.0.1:6379");
            client.ConfigSet("appendonly",Encoding.UTF8.GetBytes("yes"));   // da svaku operaciju lepi u fajl (log)

            godine = new List<string>();
            godine.Add("I");
            godine.Add("II");
            godine.Add("III");
            godine.Add("IV");
            godine.Add("V");
            godine.Add("Master");

            if (client.Get("potvrdjeniFajloviZaBeleskeBroj") == null) {
                client.Set("potvrdjeniFajloviZaBeleskeBroj", 0);
            }

            if(client.Get("potvrdjeniFajloviZaSkripteBroj") == null){
                client.Set("potvrdjeniFajloviZaSkripteBroj", 0);
            }
            
        }

        [HttpPost]
        [Route("DodajPredmet/{GodinaPredavanja}/{Predmet}")]    // administrator
        public string DodajPredmet([FromRoute] string GodinaPredavanja, [FromRoute] string Predmet) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            if (!Token(authorization)) {
                return "Nevalidan token";
            }

            if (!godine.Contains(GodinaPredavanja)) {
                return "Nevalidna godina predavanja";
            }

            var kljuc = GodinaPredavanja;

            var predmetBytes = Encoding.UTF8.GetBytes(Predmet);

            client.SAdd(kljuc,predmetBytes);

            return "Uspesno";
            
        }

        [HttpPost]
        [Route("UkloniPredmet/{GodinaPredavanja}/{Predmet}")]    // administrator
        public string UkloniPredmet([FromRoute] string GodinaPredavanja, [FromRoute] string Predmet) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            if (!Token(authorization)) {
                return "Nevalidan token";
            }

            if (!godine.Contains(GodinaPredavanja)) {
                return "Nevalidna godina predavanja";
            }

            var kljuc = GodinaPredavanja;

            var predmetBytes = Encoding.UTF8.GetBytes(Predmet);

            client.SRem(kljuc,predmetBytes);

            return "Uspesno";
            
        }


        [HttpPost]
        [Route("DodajRok/{Rok}")]    // administrator
        public string DodajRok([FromRoute] string Rok) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            if (!Token(authorization)) {
                return "Nevalidan token";
            }

            var kljuc = "rok";

            var rokBytes = Encoding.UTF8.GetBytes(Rok);

            client.SAdd(kljuc,rokBytes);

            return "Uspesno";
            
        }

        [HttpPost]
        [Route("UkloniRok/{Rok}")]    // administrator
        public string UkloniRok([FromRoute] string Rok) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            if (!Token(authorization)) {
                return "Nevalidan token";
            }

            var kljuc = "rok";

            var rokBytes = Encoding.UTF8.GetBytes(Rok);

            client.SRem(kljuc,rokBytes);

            return "Uspesno";
            
        }


        [HttpGet]
        [Route("VratiNepotvrdjeneBlankete")]
        public List<Blanket> VratiNepotvrdjeneBlankete() {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            if (!Token(authorization)) {
                return new List<Blanket>();
            }

            IEnumerable<string> keys = client.GetKeysByPattern("nepotvrdjeno:blanket:*");
            if (keys.Count() == 0) {
                return new List<Blanket>();
            }
            List<Blanket> blanketi = client.GetAll<Blanket>(keys).Values.ToList();

            if (blanketi.Count > 0) {
                foreach(var blanket in blanketi) {
                    blanket.Fajlovi = new List<byte[]>();
                    for(int i = 0; i < blanket.PutanjeDoFajlova.Count; i++) {
                        blanket.Fajlovi.Add(System.IO.File.ReadAllBytes(blanket.PutanjeDoFajlova[i]));
                        blanket.PutanjeDoFajlova[i] = blanket.PutanjeDoFajlova[i].Split("/").Last();
                    }
                }
            }

            return blanketi;

        }



        [HttpPost]
        [Route("DodajBlanket/{GodinaPredavanja}/{Predmet}/{Godina}/{Rok}")]    // administrator
        public string DodajBlanket([FromRoute] string GodinaPredavanja, [FromRoute] string Predmet, [FromRoute] string Godina, [FromRoute] string Rok, [FromBody] List<string> fajlovi) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            if (!Token(authorization)) {
                return "Nevalidan token";
            }

            if (!godine.Contains(GodinaPredavanja)) {
                return "Nevalidna godina predavanja";
            }

            if (Int32.Parse(Godina) > DateTime.UtcNow.AddYears(1).Year && Int32.Parse(Godina) < DateTime.UtcNow.AddYears(-100).Year) {
                return "Nevalidna godina";
            }

            var predmetBytes = Encoding.UTF8.GetBytes(Predmet);

            if (client.SIsMember(GodinaPredavanja, predmetBytes) == 0) {
                return "Nevalidan predmet";
            }

            var rokBytes = Encoding.UTF8.GetBytes(Rok);

            if (client.SIsMember("rok", rokBytes) == 0) {
                return "Nevalidan rok";
            }

            var keyNepotvrdjenog = "nepotvrdjeno:blanket:" + GodinaPredavanja + ":" + Predmet.Replace(" ","") + ":" + Godina + ":" + Rok;
            var keyPotvrdjenog = "blanket:" + GodinaPredavanja + ":" + Predmet.Replace(" ","") + ":" + Godina + ":" + Rok;
            var keyPotvrdjenogGodine = "blanket:" + GodinaPredavanja + ":" + Predmet.Replace(" ","");
            client.ZAdd(keyPotvrdjenogGodine, Double.Parse(Godina), Encoding.UTF8.GetBytes(Godina));

            var NepotvrdjeniBlanket = client.Get<Blanket>(keyNepotvrdjenog);
            Blanket PotvrdjeniBlanket = client.Get<Blanket>(keyPotvrdjenog);    // ako vec postoji blanket

            int br = 0;

            if (PotvrdjeniBlanket != null) {
                br = PotvrdjeniBlanket.PutanjeDoFajlova.Count;
            }

            List<string> novePutanje = new List<string>();

            for(int i = 0; i < NepotvrdjeniBlanket.PutanjeDoFajlova.Count; i++) {
                string putanja = NepotvrdjeniBlanket.PutanjeDoFajlova[i];
                if (fajlovi.Contains(putanja.Split("/").Last())) {
                    string nastavak = putanja.Split("/").Last();
                    string ekstenzija = putanja.Split("/").Last().Split(".").Last();
                    var novaPutanja = "../../db/Blanketi/"+ nastavak.Substring(0,nastavak.Length - ekstenzija.Length - 1) + br + "." + ekstenzija;
                    System.IO.File.Move(putanja,novaPutanja);
                    novePutanje.Add(novaPutanja);
                    br++;
                }
                else {
                    System.IO.File.Delete(putanja);
                }
            }

            client.Del(keyNepotvrdjenog);

            

            if (novePutanje.Count > 0) {
                 
                if (PotvrdjeniBlanket == null) {
                    NepotvrdjeniBlanket.PutanjeDoFajlova = novePutanje;
                    client.Set<Blanket>(keyPotvrdjenog, NepotvrdjeniBlanket);
                }
                else {
                    foreach (var putanja in novePutanje) {
                        PotvrdjeniBlanket.PutanjeDoFajlova.Add(putanja);
                    }
                    client.Set<Blanket>(keyPotvrdjenog, PotvrdjeniBlanket);
                }
                
            }
            return "Uspesno";
        }


        [HttpPost]
        [Route("IzmeniBlanket/{GodinaPredavanja}/{Predmet}/{Godina}/{Rok}")]    // administrator
        public string IzmeniBlanket([FromRoute] string GodinaPredavanja, [FromRoute] string Predmet, [FromRoute] string Godina, [FromRoute] string Rok, [FromBody] List<string> fajlovi) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            if (!Token(authorization)) {
                return "Nevalidan token";
            }

            if (!godine.Contains(GodinaPredavanja)) {
                return "Nevalidna godina predavanja";
            }

            var predmetBytes = Encoding.UTF8.GetBytes(Predmet);

            if (client.SIsMember(GodinaPredavanja, predmetBytes) == 0) {
                return "Nevalidan predmet";
            }

            var rokBytes = Encoding.UTF8.GetBytes(Rok);

            if (client.SIsMember("rok", rokBytes) == 0) {
                return "Nevalidan rok";
            }

            var keyPotvrdjenog = "blanket:" + GodinaPredavanja + ":" + Predmet.Replace(" ","") + ":" + Godina + ":" + Rok;
            

            var BlanketZaIzmenu = client.Get<Blanket>(keyPotvrdjenog);   

            int br = 0;

            if (BlanketZaIzmenu != null) {
                br = BlanketZaIzmenu.PutanjeDoFajlova.Count;
            }
            else {
                return "Ne postoji";
            }

            List<string> novePutanje = new List<string>();

            for(int i = 0; i < BlanketZaIzmenu.PutanjeDoFajlova.Count; i++) {
                string putanja = BlanketZaIzmenu.PutanjeDoFajlova[i];
                if (!fajlovi.Contains(putanja.Split("/").Last())) {
                    System.IO.File.Delete(putanja);
                }
                else {
                    novePutanje.Add(putanja);
                }
            }

            BlanketZaIzmenu.PutanjeDoFajlova = novePutanje;

            if (novePutanje.Count == 0) {
                 
                client.Del(keyPotvrdjenog);
                var keyPotvrdjenogGodine = "blanket:" + GodinaPredavanja + ":" + Predmet.Replace(" ","");
                client.ZRem(keyPotvrdjenogGodine, Encoding.UTF8.GetBytes(Godina));
                
            }

            client.Set(keyPotvrdjenog,BlanketZaIzmenu);

            return "Uspesno";

        }


        
        [HttpGet]
        [Route("VratiNepotvrdjeneSkripte")]
        public List<Skripta> VratiNepotvrdjeneSkripte() {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            if (!Token(authorization)) {
                return new List<Skripta>();
            }

            IEnumerable<string> keys = client.GetKeysByPattern("nepotvrdjeno:skripte:*");
            
            List<Skripta> skripte = new List<Skripta>();
            foreach(string k in keys)
            {
                var skriptaBajtovi = client.ZRange(k,0,-1);
                for(int i =0 ; i<skriptaBajtovi.GetLength(0); i++)
                {
                    var skripta = JsonSerializer.Deserialize<Skripta>(skriptaBajtovi[i]);
                    skripta.TicksDatumString = skripta.TicksDatum.ToString();
                    skripte.Add(skripta);
                }

            }

            if (skripte.Count > 0) {
                foreach(var skripta in skripte) {
                    skripta.Fajlovi = new List<byte[]>();
                    for(int i = 0; i < skripta.PutanjeDoFajlova.Count; i++) {
                        skripta.Fajlovi.Add(System.IO.File.ReadAllBytes(skripta.PutanjeDoFajlova[i]));
                        skripta.PutanjeDoFajlova[i] = skripta.PutanjeDoFajlova[i].Split("/").Last();
                    }
                }
            }

            return skripte;

        }

        [HttpPost]
        [Route("DodajSkriptu/{TicksDatumString}/{GodinaPredavanja}/{Predmet}")]    // administrator
        public string DodajSkriptu([FromRoute] string TicksDatumString,[FromRoute] string GodinaPredavanja, [FromRoute] string Predmet, [FromBody] List<string> fajlovi) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            if (!Token(authorization)) {
                return "Nevalidan token";
            }

            if (!godine.Contains(GodinaPredavanja)) {
                return "Nevalidna godina predavanja";
            }

            var predmetBytes = Encoding.UTF8.GetBytes(Predmet);

            if (client.SIsMember(GodinaPredavanja, predmetBytes) == 0) {
                return "Nevalidan predmet";
            }

            var keyNepotvrdjene = "nepotvrdjeno:skripte:" + GodinaPredavanja + ":" + Predmet.Replace(" ","");
            var keyPotvrdjene = "skripte:" + GodinaPredavanja + ":" + Predmet.Replace(" ","");


            var nepotvrdjeneSkripteBajtovi = client.ZRange(keyNepotvrdjene,0,-1);
            if(nepotvrdjeneSkripteBajtovi == null)
            {
                return "Nema nepotvrdjenih skripti";
            }

            Skripta nepotvrdjenaSkripta = null;
            int k = 0;
            for(int i =0 ; i<nepotvrdjeneSkripteBajtovi.GetLength(0); i++)
            {
                var skripta = JsonSerializer.Deserialize<Skripta>(nepotvrdjeneSkripteBajtovi[i]);
                skripta.TicksDatumString = skripta.TicksDatum.ToString();
                if(skripta.TicksDatumString == TicksDatumString)
                {
                    nepotvrdjenaSkripta = skripta;
                    k=i;
                    break;
                }
            }

            if(nepotvrdjenaSkripta == null){
                return "Nevalidna skripta";
            }

            
            List<string> novePutanje = new List<string>();
            long br = Int64.Parse(Encoding.UTF8.GetString(client.Get("potvrdjeniFajloviZaSkripteBroj")));

            for(int i = 0; i < nepotvrdjenaSkripta.PutanjeDoFajlova.Count; i++) {
                string putanja = nepotvrdjenaSkripta.PutanjeDoFajlova[i];
                if (fajlovi.Contains(putanja.Split("/").Last())) {
                    string nastavak = putanja.Split("/").Last();
                    string ekstenzija = putanja.Split("/").Last().Split(".").Last();
                    var novaPutanja = "../../db/Skripte/"+ nastavak.Substring(0,nastavak.Length - ekstenzija.Length - 1) + br + "." + ekstenzija;
                    System.IO.File.Move(putanja,novaPutanja);
                    novePutanje.Add(novaPutanja);
                    br++;
                }
                else {
                    System.IO.File.Delete(putanja);
                }
            }

            if(novePutanje.Count > 0){

                nepotvrdjenaSkripta.PutanjeDoFajlova = novePutanje;
                byte[] skriptaBytes = JsonSerializer.SerializeToUtf8Bytes(nepotvrdjenaSkripta);
                client.ZAdd(keyPotvrdjene,nepotvrdjenaSkripta.TicksDatum, skriptaBytes);
            }

            client.ZRem(keyNepotvrdjene,nepotvrdjeneSkripteBajtovi[k]);
            return "Uspesno";
        }

        [HttpPost]
        [Route("ObrisiSkriptu/{TicksDatumString}/{GodinaPredavanja}/{Predmet}")]    // administrator
        public string ObrisiSkriptu([FromRoute] string TicksDatumString,[FromRoute] string GodinaPredavanja, [FromRoute] string Predmet) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            if (!Token(authorization)) {
                return "Nevalidan token";
            }

            if (!godine.Contains(GodinaPredavanja)) {
                return "Nevalidna godina predavanja";
            }

            var predmetBytes = Encoding.UTF8.GetBytes(Predmet);

            if (client.SIsMember(GodinaPredavanja, predmetBytes) == 0) {
                return "Nevalidan predmet";
            }

            var keyPotvrdjene = "skripte:" + GodinaPredavanja + ":" + Predmet.Replace(" ","");

            byte[][] skripte = client.ZRangeByScore(keyPotvrdjene,Int64.Parse(TicksDatumString),Int64.Parse(TicksDatumString)+1,null,null);

            Skripta skripta = JsonSerializer.Deserialize<Skripta>(skripte[0]);
            for(int i = 0; i < skripta.PutanjeDoFajlova.Count; i++) {
                System.IO.File.Delete(skripta.PutanjeDoFajlova[0]);
            }

            client.ZRem(keyPotvrdjene,skripte[0]);
      
            return "Uspesno";

        }

        [HttpGet]
        [Route("VratiNepotvrdjeneBeleske")]
        public List<Beleska> VratiNepotvrdjeneBeleske() {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            if (!Token(authorization)) {
                return new List<Beleska>();
            }

            IEnumerable<string> keys = client.GetKeysByPattern("nepotvrdjeno:beleske:*");
            
            List<Beleska> beleske = new List<Beleska>();
            foreach(string k in keys)
            {
                var beleskaBajtovi = client.ZRange(k,0,-1);
                for(int i =0 ; i<beleskaBajtovi.GetLength(0); i++)
                {
                    var beleska = JsonSerializer.Deserialize<Beleska>(beleskaBajtovi[i]);
                    beleska.TicksDatumString = beleska.TicksDatum.ToString();
                    beleske.Add(beleska);
                }

            }

            if (beleske.Count > 0) {
                foreach(var beleska in beleske) {
                    beleska.Fajlovi = new List<byte[]>();
                    for(int i = 0; i < beleska.PutanjeDoFajlova.Count; i++) {
                        beleska.Fajlovi.Add(System.IO.File.ReadAllBytes(beleska.PutanjeDoFajlova[i]));
                        beleska.PutanjeDoFajlova[i] = beleska.PutanjeDoFajlova[i].Split("/").Last();
                    }
                }
            }

            return beleske;

        }

        [HttpPost]
        [Route("DodajBelesku/{TicksDatumString}/{GodinaPredavanja}/{Predmet}")]    // administrator
        public string DodajBelesku([FromRoute] string TicksDatumString,[FromRoute] string GodinaPredavanja, [FromRoute] string Predmet, [FromBody] List<string> fajlovi) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            if (!Token(authorization)) {
                return "Nevalidan token";
            }

            if (!godine.Contains(GodinaPredavanja)) {
                return "Nevalidna godina predavanja";
            }

            var predmetBytes = Encoding.UTF8.GetBytes(Predmet);

            if (client.SIsMember(GodinaPredavanja, predmetBytes) == 0) {
                return "Nevalidan predmet";
            }

            var keyNepotvrdjene = "nepotvrdjeno:beleske:" + GodinaPredavanja + ":" + Predmet.Replace(" ","");
            var keyPotvrdjene = "beleske:" + GodinaPredavanja + ":" + Predmet.Replace(" ","");


            var nepotvrdjeneBeleskeBajtovi = client.ZRange(keyNepotvrdjene,0,-1);
            if(nepotvrdjeneBeleskeBajtovi == null)
            {
                return "Nema nepotvrdjenih beleski";
            }

            Beleska nepotvrdjenaBeleska = null;
            int k = 0;
            for(int i =0 ; i<nepotvrdjeneBeleskeBajtovi.GetLength(0); i++)
            {
                var beleska = JsonSerializer.Deserialize<Beleska>(nepotvrdjeneBeleskeBajtovi[i]);
                beleska.TicksDatumString = beleska.TicksDatum.ToString();
                if(beleska.TicksDatumString == TicksDatumString)
                {
                    nepotvrdjenaBeleska = beleska;
                    k=i;
                    break;
                }
            }

            if(nepotvrdjenaBeleska == null){
                return "Nevalidna beleska";
            }

            
            List<string> novePutanje = new List<string>();
            long br = Int64.Parse(Encoding.UTF8.GetString(client.Get("potvrdjeniFajloviZaBeleskeBroj")));

            for(int i = 0; i < nepotvrdjenaBeleska.PutanjeDoFajlova.Count; i++) {
                string putanja = nepotvrdjenaBeleska.PutanjeDoFajlova[i];
                if (fajlovi.Contains(putanja.Split("/").Last())) {
                    string nastavak = putanja.Split("/").Last();
                    string ekstenzija = putanja.Split("/").Last().Split(".").Last();
                    var novaPutanja = "../../db/Beleske/"+ nastavak.Substring(0,nastavak.Length - ekstenzija.Length - 1) + br + "." + ekstenzija;
                    System.IO.File.Move(putanja,novaPutanja);
                    novePutanje.Add(novaPutanja);
                    br++;
                }
                else {
                    System.IO.File.Delete(putanja);
                }
            }

            if(novePutanje.Count > 0){

                nepotvrdjenaBeleska.PutanjeDoFajlova = novePutanje;
                byte[] beleskaBytes = JsonSerializer.SerializeToUtf8Bytes(nepotvrdjenaBeleska);
                client.ZAdd(keyPotvrdjene,nepotvrdjenaBeleska.TicksDatum, beleskaBytes);
            }

            client.ZRem(keyNepotvrdjene,nepotvrdjeneBeleskeBajtovi[k]);
            return "Uspesno";

        }

        [HttpPost]
        [Route("ObrisiBelesku/{TicksDatumString}/{GodinaPredavanja}/{Predmet}")]    // administrator
        public string ObrisiBelesku([FromRoute] string TicksDatumString,[FromRoute] string GodinaPredavanja, [FromRoute] string Predmet) {

            StringValues x;
            HttpContext.Request.Headers.TryGetValue("Authorization", out x);
            string authorization = x.ToString();

            if (!Token(authorization)) {
                return "Nevalidan token";
            }

            if (!godine.Contains(GodinaPredavanja)) {
                return "Nevalidna godina predavanja";
            }

            var predmetBytes = Encoding.UTF8.GetBytes(Predmet);

            if (client.SIsMember(GodinaPredavanja, predmetBytes) == 0) {
                return "Nevalidan predmet";
            }

            var keyPotvrdjene = "beleske:" + GodinaPredavanja + ":" + Predmet.Replace(" ","");

            byte[][] beleske = client.ZRangeByScore(keyPotvrdjene,Int64.Parse(TicksDatumString),Int64.Parse(TicksDatumString)+1,null,null);

            Skripta beleska = JsonSerializer.Deserialize<Skripta>(beleske[0]);
            for(int i = 0; i < beleska.PutanjeDoFajlova.Count; i++) {
                System.IO.File.Delete(beleska.PutanjeDoFajlova[0]);
            }

            client.ZRem(keyPotvrdjene,beleske[0]);
      
            return "Uspesno";

        }



        [HttpPost]
        [Route("PostaviSifruAdminu")]
        private void PostaviSifruAdminu([FromBody] StringReq sifra) {

            HashAlgorithm sha = SHA256.Create();
            
            byte[] result = sha.ComputeHash(Encoding.ASCII.GetBytes(sifra.Text));
            sifra.Text = Encoding.ASCII.GetString(result);
            client.Set("user:admin", sifra.Text);
            
        }

        [HttpPost]  
        [Route("Prijava")]  
        public string Prijava([FromBody] Prijavljivanje podaciPrijavljivanja) {

            if (podaciPrijavljivanja.KorisnickoIme != "admin") {
                return "Netacan username";
            }

            HashAlgorithm sha = SHA256.Create();
            
            byte[] result = sha.ComputeHash(Encoding.ASCII.GetBytes(podaciPrijavljivanja.Sifra));
            var hesiranaSifra = Encoding.ASCII.GetString(result);

            var sacuvanaSifra = client.Get<string>("user:admin");

            if (hesiranaSifra != sacuvanaSifra) {
                return "Netacna sifra";
            }

            var random = new Random((int)DateTime.UtcNow.Ticks);
            string token = "";
            for(int i = 0; i < 20; i++) {
                token += (new Random((int)DateTime.UtcNow.Ticks).Next().ToString());
            }

            client.Set("user:token", token);

            return token;

        }

        private bool Token(string token) {

            string tokenSacuvan = client.Get<string>("user:token");

            if (token == tokenSacuvan) {
                return true;
            }
            else {
                return false;
            }

        }

        
    }

    