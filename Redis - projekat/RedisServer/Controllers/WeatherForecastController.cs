﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ServiceStack.Redis;

namespace RedisServer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        //private readonly IRedisClientsManager _manager;
        //private readonly RedisClient client;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)//,IRedisClientsManager manager)
        {
            _logger = logger;
            //_manager = manager;
            //client = new RedisClient();
            //client.ConfigSet("appendonly",Encoding.UTF8.GetBytes("yes"));
        }

        [HttpGet]
        private IEnumerable<WeatherForecast> Get()
        {
            //client.BgSave();    // da sacuva na disk
            
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();

        }
    }
}
