using System.Collections.Generic;
namespace RedisServer.Models {

    public class Skripta {

        public string Naziv { get; set; }

        public string GodinaPredavanja { get; set;}

        public string Predmet { get; set; }

        public List<string> PutanjeDoFajlova { get; set; }      // slike, txt, word, pdf

        public List<byte[]> Fajlovi { get; set; }

        public long TicksDatum { get; set; }

        public string TicksDatumString { get; set; }

        public string Autor { get; set; }

    }

}