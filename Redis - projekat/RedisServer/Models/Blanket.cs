using System.Collections.Generic;

namespace RedisServer.Models {

    public class Blanket {

        public string GodinaPredavanja { get; set;}

        public string Predmet { get; set; }

        public string Godina { get; set; }

        public string Rok { get; set; }

        public List<string> PutanjeDoFajlova { get; set; }      // slike, txt, word, pdf

        public List<byte[]> Fajlovi { get; set; }

        //public string PutanjeDoFajlovaString { get; set; }

        public static string NapraviNazivFajla(string GodinaPredavanja, string Predmet, string Godina, string Rok, int Count, string fileName) {
            return GodinaPredavanja + Predmet.Replace(" ","") + Godina + Rok + Count + fileName;
        }

    }

}